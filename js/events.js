$(document).ready(function(){
	$('.custom_customize_ui_view1').checkboxradio();

	$('.custom_customize_ui_view1').on('change', function() {
		if ($(this).prop('checked'))
		{
			$(this).parents('.checkbox').prev('.select').removeClass('active').addClass('disabled');
			$(this).parents('.checkbox').next('.select').removeClass('disabled').addClass('active');
		}
		else
		{
			$(this).parents('.checkbox').prev('.select').removeClass('disabled').addClass('active');
			$(this).parents('.checkbox').next('.select').removeClass('active').addClass('disabled');
		}
	});
	
	$('.js-main-menu').on('click', function(){
		
	})
	
	$('.js-popup').on('click', function()
	{
		var popup_id = $(this).data('popup');
		$('.is_open').removeClass('is_open');
		$('#' + popup_id).addClass('is_open');
		$('.box_shadow').addClass('active');
		$('body').addClass('noscroll');
		$('#' + popup_id + ' .body').css({height: $(window).height() - 102 + 'px'});
		return false;
	});
	
	if (screen.width < 992)
	{
		$( window ).resize(function(){
			$('.popup .body').css({height: $(window).height() - 102 + 'px'});
		});
	}
	
	$('.box-shadow').on('click', function()
	{
		$(this).removeClass('active');
		$('.popup').removeClass('is_open');
	})
	
	$('.js-close-popup').on('click', function()
	{
		$('.is_open').removeClass('is_open');
		$('.box_shadow').removeClass('active');
		$('body').removeClass('noscroll');
		return false;
	});
	
	$('.js-auth-popup').on('click', function()
	{
		var popup = $(this).parents('.popup');
		//popup.removeClass('is_open');
		if (popup.find('.switch input[type="checkbox"]').prop('checked'))
		{
			var content = $('.popup[data-auth="register"] .body').html();
			popup.find('.body').html(content);
			//$('#enter_on_site').addClass('is_open');
			//$('#register_on_site .custom_customize_ui_view1').prop('checked',true).checkboxradio('refresh');
			//$('#enter_on_site .custom_customize_ui_view1').prop('checked',false).checkboxradio('refresh');
		}
		if (!popup.find('.switch input[type="checkbox"]').prop('checked'))
		{
			var content = $('.popup[data-auth="enter"] .body').html();
			popup.find('.body').html(content);
			//$('#register_on_site').addClass('is_open');
			//$('#enter_on_site .custom_customize_ui_view1').prop('checked',false).checkboxradio('refresh');
			//$('#register_on_site .custom_customize_ui_view1').prop('checked',true).checkboxradio('refresh');
		}
	});
	
	$('.js-cover').on('click', function()
	{
		var parent = $(this).parents('.js-wrap_list_points');
		var list = parent.find('.js-list');
		
		if ($(this).hasClass('uncover'))
		{
			$(this).removeClass('uncover');
			$(this).find('span').text('Развернуть');
		}
		else
		{
			$(this).addClass('uncover');
			$(this).find('span').text('Свернуть');
		}
		
		if (list.data('hide') == true)
		{
			list.data('hide', false);
			list.fadeIn(200);
		}
		else
		{
			list.data('hide', true);
			list.fadeOut(200);
		}
	});
	
	function clear_search_field()
	{
		$('.search [data-type="reset"]').on('click', function()
		{
			$(this).parents('form').find('.field').removeClass('not_empty');
			$(this).attr('data-type', 'submit');
			$(this).parents('form').trigger('reset');
		})
		return false;
	}
	
	$('.search input').on('input', function()
	{
		if ($(this).val().length > 0)
		{
			$(this).parents('.field').addClass('not_empty');
			$(this).parents('form').find('.state').attr('data-type', 'reset');
		}
		else
		{
			$(this).parents('.field').removeClass('not_empty');
			$(this).parents('form').find('.state').attr('data-type', 'submit');
		}
		clear_search_field();
		return false;
	})
});